<?php

namespace App\Controller\Users;

use App\Document\Product\Product;
use App\Document\Record\Record;
use App\Document\Users\Patient;
use App\Entity\Product\ProductMeasure;
use App\Entity\Users\Patient\DailyDose;
use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use function Sodium\add;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;

class PatientController extends FOSRestController
{
    /**
     * @Annotations\Post("/signup")
     * @ParamConverter("request", converter="fos_rest.request_body")
     */
    public function create(Patient $request)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $repository = $dm->getRepository(Patient::class);
        $patient = $repository->findOneBy(['email' => $request->getEmail()]);
        if ($patient) {
            return new Response('User wtih this email is already exist', 400);
        }
        $dm->persist($request);
        $dm->flush();
        return new Response($request->getId());
    }

    /**
     * @Annotations\Post("/signin")
     * @ParamConverter("request", converter="fos_rest.request_body")
     * @param Patient request
     * @return Response
     */
    public function login(Patient $request)
    {
        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $repository = $dm->getRepository(Patient::class);
        $patient = $repository->findOneBy(['email' => $request->getEmail()]);
        if (! $patient) {
            return new Response('No users with email ' . $request->getEmail(), 404);
        }

        return new Response($patient->getId());
    }

    /**
     * @Annotations\Get("/api/patient")
     */
    public function getUserData(Request $request)
    {
        $apiKey = $request->headers->get('Authorization');
        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $repository = $dm->getRepository(Patient::class);
        $patient = $repository->find($apiKey);

        return $this->json($patient);
    }

    /**
     * @Annotations\Post("/api/patient")
     * @ParamConverter("request", converter="fos_rest.request_body")
     * @param Patient $request
     * @return Response
     */
    public function update(Patient $request)
    {
        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($request);
        $dm->flush();
        return $this->json($request);
    }

    /**
     * @Annotations\Get("/api/patient/stats")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function stats(Request $request)
    {
        $apiKey = $request->headers->get('Authorization');
        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $repository = $dm->getRepository(Patient::class);
        /** @var Patient $patient */
        $patient = $repository->find($apiKey);
        $dailyDose = DailyDose::createInstance((array)$patient->getDailyDose());
        $recordsRepo = $dm->getRepository(Record::class);
        $productRepo = $dm->getRepository(Product::class);
        $records = $recordsRepo->findBy(['customerId' => $patient->getId()]);
        $today = new \DateTime();
        $tomorrow = new \DateTime('tomorrow');
        $recs = array_filter($records, function (Record $rec) use ($today, $tomorrow) {

            $today->setTime(0,0,0);
            return  ($rec->getDate()->sec - $today->getTimestamp()) > 0 && ($rec->getDate()->sec - $tomorrow->getTimestamp()) < 0;
        });
        $productIds = array_map(function (Record $r) {
            return $r->getProductId();
        }, $recs);
        $products = $productRepo->findBy(['id' => ['$in' => $productIds]]);
        $productMeasures = array_map(function (Product $p) {
            return $p->getProductMeasure();
        }, $products);
        $proteins = array_map(function (ProductMeasure $p) {
            return $p->getProteins();
        }, $productMeasures);
        $fats = array_map(function (ProductMeasure $p) {
            return $p->getFats();
        }, $productMeasures);
        $calories = array_map(function (ProductMeasure $p) {
            return $p->getCalories();
        }, $productMeasures);
        $carb = array_map(function (ProductMeasure $p) {
            return $p->getCarbohydrates();
        }, $productMeasures);
        $insulin = array_map(function (Record $p) {
            return $p->getShortInsuiln();
        }, $recs);

        return $this->json([
            'prot' => round((float)array_sum($proteins) / $dailyDose->getProteins() * 100),
            'fat' => round((float)array_sum($fats) / $dailyDose->getFats() * 100),
            'carb' => round((float)array_sum($carb) / $dailyDose->getCarbohydrates() * 100),
            'ins' => round((float)array_sum($insulin) / $dailyDose->getInsulin() * 100),
        ]);
    }
}
