<?php

namespace App\Controller\Product;


use App\Document\Product\Product;
use App\Service\Product\ProductService;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;

class ProductController extends FOSRestController
{
    /**
     * @Annotations\Post("/api/product")
     * @ParamConverter("product", converter="fos_rest.request_body")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function create(Request $request, Product $product)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $apiKey = $request->headers->get('Authorization');
        $product->setCustomerId($apiKey);
        $dm->persist($product);
        $dm->flush();
        return new Response($product->getId());
    }

    /**
     * @Annotations\Get("/api/product")
     * @param Request $request
     * @param ProductService $productService
     * @return Response
     */
    public function read(Request $request, ProductService $productService)
    {
        return $this->json($productService->getProductsByCustomerId($request->headers->get('Authorization')));
    }
}