<?php

namespace App\Controller\Record;


use App\Document\Product\Product;
use App\Document\Record\Record;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Stubs\DocumentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations;

class RecordController extends FOSRestController
{
    /**
     * @Annotations\Post("/api/record")
     * @ParamConverter("record", converter="fos_rest.request_body")
     * @param Request $request
     * @param Record $record
     * @return Response
     */
    public function create(Request $request, Record $record)
    {
        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $productRepo = $dm->getRepository(Product::class);
        /** @var Product $product */
        $product = $productRepo->find($record->getProductId());
        if ($product && !$record->getBreadPoints()) {
            $record->setBreadPoints($product->getBreadPoints());
        }
        $apiKey = $request->headers->get('Authorization');
        $record->setCustomerId($apiKey);
        $dm->persist($record);
        $dm->flush();
        return new Response($record->getId());
    }

    /**
     * @Annotations\Get("/api/records")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function read(Request $request)
    {
        $apiKey = $request->headers->get('Authorization');
        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $recordsRepo = $dm->getRepository(Record::class);
        $productRepo = $dm->getRepository(Product::class);
        $today = new \DateTime();
        $tomorrow = new \DateTime('tomorrow');
        if ($request->get('date')) {
            $today = new \DateTime($request->get('date'));
            $tomorrow = (new \DateTime($request->get('date')))->modify('+1 day');
        }
        $records = $recordsRepo->findBy(['customerId' => $apiKey], ['date' => 'desc'], $request->get('limit'));
        $recs = array_filter($records, function (Record $rec) use ($today, $tomorrow) {
            $today->setTime(0,0,0);
            return  ($rec->getDate()->sec - $today->getTimestamp()) > 0 && ($rec->getDate()->sec - $tomorrow->getTimestamp()) < 0;
        });
        $result = array_map(function (Record $r) use ($productRepo) {
            /** @var Product $product */
            $product = $productRepo->find($r->getProductId());
            return [
                'title' => $product->getName(),
                'record' => $r
            ];
        }, $recs);
        return $this->json($result);
    }
}