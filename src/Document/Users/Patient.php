<?php

namespace App\Document\Users;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Patient
 * @package App\Document
 * @MongoDB\Document("patient")
 */
class Patient
{
    /**
     * @MongoDB\Id()
     * @Serializer\Type("string")
     */
    public $id;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $username;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $password;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $email;

    /**
     * @MongoDB\Field(type="float")
     * @Serializer\Type("float")
     */
    public $weight;

    /**
     * @MongoDB\Field(type="float")
     * @Serializer\Type("float")
     */
    public $koef;

    /**
     * @MongoDB\Field(type="hash")
     * @Serializer\Type("array<string, string>")
     */
    public $compensation;

    /**
     * @MongoDB\Field(type="hash")
     * @Serializer\Type("array<string, string>")
     */
    public $dailyDose;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getKoef()
    {
        return $this->koef;
    }

    /**
     * @param mixed $koef
     */
    public function setKoef($koef): void
    {
        $this->koef = $koef;
    }

    /**
     * @return mixed
     */
    public function getCompensation()
    {
        return $this->compensation;
    }

    /**
     * @param mixed $compensation
     */
    public function setCompensation($compensation): void
    {
        $this->compensation = $compensation;
    }

    /**
     * @return mixed
     */
    public function getDailyDose()
    {
        return (object)$this->dailyDose;
    }

    /**
     * @param mixed $dailyDose
     */
    public function setDailyDose($dailyDose): void
    {
        $this->dailyDose = $dailyDose;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}
