<?php

namespace App\Document\Record;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\HasLifecycleCallbacks;
use Doctrine\ODM\MongoDB\Mapping\Annotations\PrePersist;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Record
 * @package App\Document
 * @HasLifecycleCallbacks
 * @MongoDB\Document("record")
 */
class Record
{
    /**
     * @MongoDB\Id()
     */
    public $id;

    /**
     * @Serializer\Type("float")
     * @MongoDB\Field(type="float")
     */
    public $glucoseLevel;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $productId;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $customerId;

    /**
     * @Serializer\Type("float")
     * @MongoDB\Field(type="float")
     */
    public $breadPoints;

    /**
     * @Serializer\Type("float")
     * @MongoDB\Field(type="float")
     */
    public $shortInsuiln;

    /**
     * @MongoDB\Field(type="timestamp")
     */
    public $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGlucoseLevel()
    {
        return $this->glucoseLevel;
    }

    /**
     * @param mixed $glucoseLevel
     */
    public function setGlucoseLevel($glucoseLevel): void
    {
        $this->glucoseLevel = $glucoseLevel;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getBreadPoints()
    {
        return $this->breadPoints;
    }

    /**
     * @param mixed $breadPoints
     */
    public function setBreadPoints($breadPoints): void
    {
        $this->breadPoints = $breadPoints;
    }

    /**
     * @return mixed
     */
    public function getShortInsuiln()
    {
        return $this->shortInsuiln;
    }

    /**
     * @param mixed $shortInsuiln
     */
    public function setShortInsuiln($shortInsuiln): void
    {
        $this->shortInsuiln = $shortInsuiln;
    }

    /**
     * @return \MongoTimestamp
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /** @PrePersist */
    public function doStuffOnPrePersist(\Doctrine\ODM\MongoDB\Event\LifecycleEventArgs $eventArgs)
    {
        $this->date = date_timestamp_get(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId): void
    {
        $this->customerId = $customerId;
    }
}
