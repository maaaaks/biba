<?php

namespace App\Document\Product;

use App\Entity\Product\ProductMeasure;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Product
 * @package App\Document
 * @MongoDB\Document("product")
 */
class Product
{
    /**
     * @MongoDB\Id()
     */
    public $id;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $name;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $customerId;

    /**
     * @Serializer\Type("float")
     * @MongoDB\Field(type="float")
     */
    public $breadPoints;

    /**
     * @Serializer\Type("string")
     * @MongoDB\Field(type="string")
     */
    public $category;

    /**
     * @Serializer\Type("array<string, string>")
     * @MongoDB\Field(type="hash")
     */
    public $productMeasure;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getBreadPoints()
    {
        return $this->breadPoints;
    }

    /**
     * @param float $breadPoints
     */
    public function setBreadPoints($breadPoints): void
    {
        $this->breadPoints = $breadPoints;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return ProductMeasure
     * @throws \Exception
     */
    public function getProductMeasure()
    {
        return ProductMeasure::createInstance((array)$this->productMeasure);
    }

    /**
     * @param array $productMeasure
     */
    public function setProductMeasure(array $productMeasure): void
    {
        $this->productMeasure = $productMeasure;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId): void
    {
        $this->customerId = $customerId;
    }
}
