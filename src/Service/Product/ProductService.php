<?php

namespace App\Service\Product;

use App\Document\Product\Product;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * Class ProductService
 *
 * @package App\Service\Product
 */
class ProductService
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * ProductService constructor.
     *
     * @param DocumentManager $dm
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param string $customerId
     * @return Product[]|array|\Documents\Product[]
     */
    public function getProductsByCustomerId(string $customerId)
    {
        $productRepository = $this->dm->getRepository(Product::class);
        return $productRepository->findBy(['customerId' => $customerId]);
    }
}
