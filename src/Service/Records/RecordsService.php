<?php

namespace App\Service\Records;

use Doctrine\ODM\MongoDB\DocumentManager;

class RecordsService
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * ProductService constructor.
     *
     * @param DocumentManager $dm
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }
}
