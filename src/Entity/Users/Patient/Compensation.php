<?php

namespace App\Entity\Users\Patient;

class Compensation
{
    const KEYS = [
        'targetGlucose',
        'lowBridge',
        'highBridge',
        'koefOfSens'
    ];
    private $targetGlucose;

    private $lowBridge;

    private $highBridge;

    private $koefOfSens;

    private function __construct($data)
    {
        $this->targetGlucose = $data['targetGlucose'];
        $this->lowBridge = $data['lowBridge'];
        $this->highBridge = $data['highBridge'];
        $this->koefOfSens = $data['koefOfSens'];
    }

    /**
     * @return mixed
     */
    public function getTargetGlucose()
    {
        return $this->targetGlucose;
    }

    /**
     * @param mixed $targetGlucose
     */
    public function setTargetGlucose($targetGlucose): void
    {
        $this->targetGlucose = $targetGlucose;
    }

    /**
     * @return mixed
     */
    public function getLowBridge()
    {
        return $this->lowBridge;
    }

    /**
     * @param mixed $lowBridge
     */
    public function setLowBridge($lowBridge): void
    {
        $this->lowBridge = $lowBridge;
    }

    /**
     * @return mixed
     */
    public function getHighBridge()
    {
        return $this->highBridge;
    }

    /**
     * @param mixed $highBridge
     */
    public function setHighBridge($highBridge): void
    {
        $this->highBridge = $highBridge;
    }

    /**
     * @return mixed
     */
    public function getKoefOfSens()
    {
        return $this->koefOfSens;
    }

    /**
     * @param mixed $koefOfSens
     */
    public function setKoefOfSens($koefOfSens): void
    {
        $this->koefOfSens = $koefOfSens;
    }

    /**
     * @param array $data
     * @return Compensation
     * @throws \Exception
     */
    public static function createInstance(array $data)
    {
        $real = array_keys($data);
        $real = sort($real);
        $expected = self::KEYS;
        $expected = sort($expected);
        if ($expected !== $real) {
            throw new \Exception('Incorrect product measure');
        }
        return new self($data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_combine(self::KEYS, [
            $this->targetGlucose,
            $this->lowBridge,
            $this->highBridge,
            $this->koefOfSens
        ]);
    }
}
