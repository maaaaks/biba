<?php

namespace App\Entity\Users\Patient;

class DailyDose
{
    const KEYS = [
        'proteins',
        'carbohydrates',
        'fats',
        'insulin'
    ];

    /**
     * @var float
     */
    private $proteins;

    /**
     * @var float
     */
    private $carbohydrates;

    /**
     * @var float
     */
    private $fats;

    /**
     * @var float
     */
    private $insulin;

    private function __construct($data)
    {
        $this->carbohydrates = $data['carbohydrates'];
        $this->fats = $data['fats'];
        $this->proteins = $data['proteins'];
        $this->insulin = $data['insulin'];
    }

    /**
     * @return float
     */
    public function getProteins()
    {
        return $this->proteins;
    }

    /**
     * @param float $proteins
     */
    public function setProteins($proteins): void
    {
        $this->proteins = $proteins;
    }

    /**
     * @return float
     */
    public function getCarbohydrates()
    {
        return $this->carbohydrates;
    }

    /**
     * @param float $carbohydrates
     */
    public function setCarbohydrates($carbohydrates): void
    {
        $this->carbohydrates = $carbohydrates;
    }

    /**
     * @return float
     */
    public function getFats()
    {
        return $this->fats;
    }

    /**
     * @param float $fats
     */
    public function setFats($fats): void
    {
        $this->fats = $fats;
    }

    /**
     * @return float
     */
    public function getInsulin(): float
    {
        return $this->insulin;
    }

    /**
     * @param float $insulin
     */
    public function setInsulin(float $insulin): void
    {
        $this->insulin = $insulin;
    }


    /**
     * @param array $data
     * @return DailyDose
     * @throws \Exception
     */
    public static function createInstance(array $data)
    {
        $real = array_keys($data);
        $real = sort($real);
        $expected = self::KEYS;
        $expected = sort($expected);
        if ($expected !== $real) {
            throw new \Exception('Incorrect product measure');
        }
        return new self($data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_combine(self::KEYS, [
            $this->proteins,
            $this->carbohydrates,
            $this->fats,
            $this->insulin
        ]);
    }
}
